package com.jossemar.customer.mapper;

import com.jossemar.customer.domain.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
    com.jossemar.customer.model.Customer toModel(Customer customer);
    Customer toDocument(com.jossemar.customer.model.Customer customer);
}
