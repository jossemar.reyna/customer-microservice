package com.jossemar.customer.controller;

import com.jossemar.customer.api.CustomerApi;
import com.jossemar.customer.model.Customer;
import com.jossemar.customer.model.Query;
import com.jossemar.customer.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@RequestMapping("/api/microservice/1.0.0")
@RestController
@Slf4j
public class CustomerController implements CustomerApi {

    @Autowired
    private CustomerService service;

    @Override
    public Mono<ResponseEntity<Void>> delete(String id, ServerWebExchange exchange) {
        return service.deleteById(id)
                .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @Override
    public Mono<ResponseEntity<Customer>> findById(String id, ServerWebExchange exchange) {
        return service.findById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @Override
    public Mono<ResponseEntity<Customer>> save(Mono<Customer> customer, ServerWebExchange exchange) {
        return service.save(customer)
                .map(ResponseEntity.status(HttpStatus.CREATED)::body);
    }

    @Override
    public Mono<ResponseEntity<Customer>> update(String id, Mono<Customer> customer, ServerWebExchange exchange) {
        return service.update(customer, id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @Override
    public Mono<ResponseEntity<Customer>> findByParamGet(String email, String dni, ServerWebExchange exchange) {
        return service.findByDniAndEmail(dni, email)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @Override
    public Mono<ResponseEntity<Customer>> findByParamPost(Mono<Query> query, ServerWebExchange exchange) {
        return query.flatMap(c -> service.findByDniAndEmail(c.getDni(), c.getEmail()))
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
