package com.jossemar.customer.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@Builder
@Document
public class Customer {
    @Id
    private String id;
    private String names;
    private String fatherLastName;
    private String motherLastName;
    @Indexed(unique = true)
    private String dni;
    @Indexed(unique = true)
    private String email;
    @Indexed(unique = true, sparse = true)
    private String phone;
    private String type;
    @CreatedDate
    private Instant createdAt;
    @LastModifiedDate
    private Instant updatedAt;
}
