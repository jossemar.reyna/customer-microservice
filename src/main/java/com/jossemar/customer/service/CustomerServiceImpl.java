package com.jossemar.customer.service;

import com.jossemar.customer.mapper.CustomerMapper;
import com.jossemar.customer.model.Customer;
import com.jossemar.customer.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    private CustomerRepository repository;
    @Autowired
    private CustomerMapper mapper;

    @Override
    public Mono<Customer> save(Mono<Customer> customer) {
        return customer
                .map(mapper::toDocument)
                .flatMap(repository::save)
                .map(mapper::toModel);
    }

    @Override
    public Mono<Customer> findById(String id) {
        return repository.findById(id)
                .map(mapper::toModel);
    }

    @Override
    public Mono<Customer> update(Mono<Customer> customer, String id) {
        return save(findById(id)
                .flatMap(c -> customer.doOnNext(x -> x.setCreatedAt(c.getCreatedAt())))
                .doOnNext(e -> e.setId(id)));
    }

    @Override
    public Mono<Void> deleteById(String id) {
        return findById(id)
                .map(mapper::toDocument)
                .flatMap(repository::delete);
    }

    @Override
    public Mono<Customer> findByDniAndEmail(String dni, String email) {
        return repository.findByDniAndEmail(dni, email)
                .map(mapper::toModel);
    }

    @Override
    public Flux<Customer> findAll() {
        return repository.findAll()
                .map(mapper::toModel);
    }
}
