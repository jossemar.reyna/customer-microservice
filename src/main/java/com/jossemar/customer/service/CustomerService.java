package com.jossemar.customer.service;

import com.jossemar.customer.model.Customer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CustomerService {
    Mono<Customer> save(Mono<Customer> customer);
    Mono<Customer> findById(String id);
    Mono<Customer> update(Mono<Customer> customer, String id);
    Mono<Void> deleteById(String id);
    Mono<Customer> findByDniAndEmail(String dni, String email);
    Flux<Customer> findAll();
}
